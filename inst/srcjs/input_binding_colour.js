var colourBinding = new Shiny.InputBinding();
$.extend(colourBinding, {
  find: function(scope) {
    // Check if colour plugin is loaded
    if (!$.fn.spectrum)
      return [];
    return $(scope).find('input.shiny-colour-input');
  },
  getValue: function(el) {
    return $(el).val();
  },
  setValue: function(el, value) {
    $(el).spectrum('set', value);
  },
  subscribe: function(el, callback) {
    $(el).on("change.colourBinding", function(e) {
      callback(true);
    });
  },
  unsubscribe: function(el) {
    $(el).off(".colourBinding");
  },
  initialize : function(el) {
    var $el = this._jqid(el.id);  // for some reason using $(el) doesn't work
    //this is a hack to make Dean's colourinput options kind of work for this
    var showPalette;
    var showPaletteOnly;
    if ($el.attr('data-paletteType')=='square') {
      showPalette=false;
      showPaletteOnly=false;
    }else if ($el.attr('data-paletteType')=='limited') {
      showPalette=true;
      showPaletteOnly=true;
    }else{
      showPalette=true;
      showPaletteOnly=false;
    }
    
    var showAlpha,palette,preferredFormat;
    if($el.attr('data-allow-transparent')==="true") {
      showAlpha=true;
    } else {
      showAlpha=false;
    }

    if($el.attr('data-return-name')==="true") {
      preferredFormat='name';
    }else{
      preferredFormat='hex8';
    }



    var opts = {
      //changeDelay      : 0,
      showAlpha        : showAlpha,
      showPalette      : showPalette,
      showPaletteOnly  : showPaletteOnly,
      //palette          : $el.attr('data-allowed-cols'),
      preferredFormat  : preferredFormat 
    };

    // initialize the colour picker
    $el.spectrum(opts);

    this.setValue(el, $el.attr('data-init-value'));

    // save the initial settings so that we can restore them later
    $el.data('init-opts', $.extend(true, {}, $el.spectrum('option')));
  },
  // update the colour input
  receiveMessage: function(el, data) {
    var $el = $(el);

    if (data.hasOwnProperty('paletteType')) {
      if (data.palette==='square') {
        $el.spectrum("option","showPallette",false);
        $el.spectrum("option","showPaletteOnly",false);
      }else if (data.palette==='limited') {
        $el.spectrum("option","showPalette",true);
        $el.spectrum("option","showPaletteOnly",true);
      }else{
        $el.spectrum("option","showPalette",true);
        $el.spectrum("option","showPaletteOnly",false);
      }
    }
 //   if (data.hasOwnProperty('allowedCols')) {
 //     $el.spectrum('option', 'palette',data.allowedCols);
 //   }
    if (data.hasOwnProperty('allowTransparent')) {
      if($el.attr('data-allow-transparent')==="true") {
        showAlpha=true;
      } else {
        showAlpha=false;
      }
      $el.spectrum('option', 'showAlpha',showAlpha);
    }
    
    if (data.hasOwnProperty('returnName')) {
      if($el.attr('data-return-name')==="true") {
        preferredFormat='name';
      }else{
        preferredFormat='hex8';
      }
      $el.spectrum('option',  'preferredFormat' , preferredFormat);
    }
    if (data.hasOwnProperty('value')) {
      this.setValue(el, data.value);
    }
    if (data.hasOwnProperty('label')) {
      this._getContainer(el).find('label[for="' + el.id + '"]').text(data.label);
    }
    if (data.hasOwnProperty('showColour')) {
      $el.spectrum('option', 'showColour' , data.showColour );
    }

    $el.trigger("change");
  },
  getRatePolicy : function() {
    return {
      policy: 'debounce',
      delay: 250
    };
  },
  // Get the shiny input container
  _getContainer : function(el) {
    return $(el).closest(".shiny-input-container");
  },
  _jqid : function(id) {
    return $("#" + id.replace( /(:|\.|\[|\]|,)/g, "\\$1" ));
  }
});

Shiny.inputBindings.register(colourBinding);
